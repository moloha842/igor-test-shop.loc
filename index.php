<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

require __DIR__ . '/vendor/autoload.php';

//подключаю шаблонизатор Twig
$loader = new FilesystemLoader('templates');
$view = new Environment($loader);

//подключаю роутер Slim
$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) use ($view) {
    $body = $view->render('index.twig');
    $response->getBody()->write($body);
    return $response;
});

$app->get('/about', function (Request $request, Response $response, $args) use ($view) {
    $body = $view->render('about.twig',[
        'name'=> 'Молодик Игорь'
    ]);
    $response->getBody()->write($body);
    return $response;
});

$app->get('/{url_key}', function (Request $request, Response $response, $args) use ($view) {
    $body = $view->render('item.twig',[
        'url_key' => $args['url_key']
    ]);
    $response->getBody()->write($body);
    return $response;
});

$app->run();
//echo 'qwertyu';